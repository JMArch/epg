#EPG
高效的通用连接池代理服务，目前计划支持mysql、redis。 epg 为英文单词express proxy as  my data server guard的递归缩写。

##Dependence Dev Tool
    goxc:
    godep:
    mysql:
    
##Install
Install the Dev tool first
```
    $ git clone https://git.oschina.net/JMArch/epg.git
    $ cd epg
    $ make
    $ cd bin
    $ ./epg
```
Make Package:
```
    $ make bin
```
Make New Version:
```
    $ make newver
```

##Architecture
 [整体设计概要](./doc/整体设计概要.pdf)  
 [连接池服务概要设计](./doc/连接池服务概要设计.pdf)  
 [mysql连接池连接对象映射结构概要](./doc/mysql连接池连接对象映射结构概要.pdf)  
 [MYSQL连接创建时序](./doc/MYSQL连接创建时序.pdf)  

##Roadmap
 [roadmap](./ROADMAP.md)
 
##参数配置说明
参数配置获取流程： 
> 读取本地配置文件 /etc/epg/epg.toml 获取consul地址及服务监听地址等。 
> 从consul中的path读取配置信息 
> 监听consul中配置的改变，动态更改本地的配置 

##部署
0. 依赖consul, 请先安装并配置好consul.(每台机器起一个consul agent ...)
1. 连接池一共包括二个部份,主程序(epg)和集群管理中心(epg-mc)，主程序可单独安装使用
2. 由于epg不对查询进行解析,所以不能区分读写,主程序(epg)的部署需要分为两个: mysql_master,mysql_slaves;
同一个epg进程只能都配置master或slaves节点. 多个slaves会进行权重轮询调度.
3. ![epg mysql nodes conf](./doc/epg_node.png)

4. 整体架构及部署. 
5. ![epg mysql arch](./doc/epg_arch.png)


##使用注意
1. 主从实例需要分开,代理到同一个连接池的多个Mysql实例中不能同时包括同一个库的主和从；
2. 多个Mysql实例中的库结构应该一致(数据库名/数据库表结构),否则可能会操作失败；
3. 同一主库的多个Slave代理到同一个连接池时, 会按配置的权重进行权重轮询调度；
4. 由于是集中式代理,用户权限会从一个实例的mysql.user表拉取数据, 进行权限鉴定；修改权限之后需要重新拉取(api/single)
5. 不支持跨库查询(当前是A库, Query语句中使用B.table进行查询);


