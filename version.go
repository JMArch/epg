package main

import (
	"fmt"
	"runtime"
)

var banner = ` 
---------------------------------------------------------+
                       EPG Proxy
	Connection Pool Proxy For Mysql!
Version:	%v
BuildTime:	%v
GitHash:	%v
GOVersion:	%v
---------------------------------------------------------+
`

//VERSION
var (
	VERSION    = "0.0.1"
	BUILD_DATE = "0000-00-00 00:00:01"
	GitHash    = "000000"
)

func displayVersion() string {
	return fmt.Sprintf(banner, VERSION, BUILD_DATE, GitHash, runtime.Version())
}
