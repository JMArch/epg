package main

import (
	"epg/log"
	"epg/privilege"
	"flag"
	"fmt"
	"time"
)

var logfile string
var dsn string

func init() {
	flag.StringVar(&logfile, "logfile", "auth.log", "file to log")
	flag.StringVar(&dsn, "dsn", "", "mysql auth dsn")
	flag.Parse()
}

func main() {
	testInitAuthChecker()
}
func testInitAuthChecker() {
	//epgproxy_srd:u71kr46H@tcp(10.17.30.88:6008)/mysql
	logger, err := log.New("debug", logfile)
	if err != nil {
		fmt.Println(err.Error())
	}
	log.SetLogger(logger)
	//privilege.InitAuthChecker("epgproxy_srd:u71kr46H@tcp(10.17.30.88:6008)/mysql")
	//privilege.InitAuthChecker("dev:jmdevcd@tcp(192.168.16.31:6006)/mysql")
	privilege.InitAuthChecker(dsn)
	time.Sleep(time.Second * 5)
}
