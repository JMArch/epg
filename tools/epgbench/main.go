package main

import (
	"os"

	"github.com/urfave/cli"
)

func main() {
	app := cli.NewApp()
	app.Name = "Epg Proxy Bench"
	app.Usage = "Bench the epg service with a lots of connections"
	app.Version = "0.0.3"
	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name: "host,H", Value: "127.0.0.1", Usage: "mysql host",
		},
		cli.StringFlag{
			Name: "port,p", Value: "6603", Usage: "mysql port",
		},
		cli.StringFlag{
			Name: "user,u", Value: "root", Usage: "mysql user",
		},
		cli.StringFlag{
			Name: "passwd,P", Value: "", Usage: "mysql passwd",
		},
		cli.StringFlag{
			Name: "database,db", Value: "", Usage: "mysql database",
		},
		cli.StringFlag{
			Name: "query,q", Value: "", Usage: "mysql query, only support SELECT * FROM TABLE WHERE F1=?",
		},
		cli.IntFlag{
			Name: "concurrent,c", Value: 1024, Usage: "mysql concurrent query num",
		},
		cli.IntFlag{
			Name: "connectionnum,n", Value: 1024 * 5, Usage: "mysql connection pool ",
		},
		cli.IntFlag{
			Name: "freq,f", Value: 3000, Usage: "mysql ping freq(ms) ",
		},
	}
	app.Action = entry
	app.Run(os.Args)
}
