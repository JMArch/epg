package conf

import (
	"fmt"
	"testing"
	"time"

	"github.com/BurntSushi/toml"
	"github.com/stretchr/testify/assert"
)

func TestLimit(t *testing.T) {
	src := `
DefaultPer = 30
[UserLimit] 
  "test_1" = 20
[UserNodeLimit]
 [UserNodeLimit.test_1]
  "node_name_1" = 10
	`

	var lc limitConf
	err := toml.Unmarshal([]byte(src), &lc)
	assert.Nil(t, err)

	Limit.Update(lc)

	user := "test_1"
	alias := "node_name_1"

	Limit.ChangeNodeConf(alias, 40)

	assert.Nil(t, get(user, alias))
	assert.Nil(t, get(user, alias))
	assert.Nil(t, get(user, alias))
	assert.Nil(t, get(user, alias))
	assert.NotNil(t, get(user, alias))

	put(user, alias)
	assert.Nil(t, get(user, alias))

	alias = "node_name_2"
	Limit.ChangeNodeConf(alias, 10)
	assert.Nil(t, get(user, alias))
	assert.Nil(t, get(user, alias))
	assert.NotNil(t, get(user, alias))
	put(user, alias)
	assert.Nil(t, get(user, alias))

	Limit.ChangeNodeConf(alias, 20)
	assert.Nil(t, get(user, alias))
	assert.Nil(t, get(user, alias))
	assert.NotNil(t, get(user, alias))

	src = `
DefaultPer = 30
[UserLimit] 
  "test_1" = 10
[UserNodeLimit]
 [UserNodeLimit.test_1]
  "node_name_1" = 10
	`
	err = toml.Unmarshal([]byte(src), &lc)
	assert.Nil(t, err)

	Limit.Update(lc)

	assert.NotNil(t, get(user, alias))
	put(user, alias)
	put(user, alias)
	put(user, alias)
	assert.Nil(t, get(user, alias))
	assert.NotNil(t, get(user, alias))

	Limit.ChangeNodeConf(alias, 10)
	user = "test_2"
	assert.Nil(t, get(user, alias))
	assert.Nil(t, get(user, alias))
	assert.Nil(t, get(user, alias))
	assert.NotNil(t, get(user, alias))

	/*
		assert.True(t, Limit.Get("test", "node", 20))
		assert.True(t, Limit.Get("test", "node", 20))
		assert.False(t, Limit.Get("test", "node", 20))
		assert.True(t, Limit.Get("test", "node", 30))

		Limit.Put("test", "node")
		Limit.Put("test", "node")
		assert.True(t, Limit.Get("test", "node", 20))

		assert.True(t, Limit.Get("test", "no_exist", 20))
		assert.True(t, Limit.Get("test", "no_exist", 20))
		assert.True(t, Limit.Get("test", "no_exist", 20))
		assert.True(t, Limit.Get("test", "no_exist", 20))
		assert.False(t, Limit.Get("test", "no_exist", 20))

		assert.True(t, Limit.Get("no_exist", "no_exist", 20))
		assert.True(t, Limit.Get("no_exist", "no_exist", 20))
		assert.True(t, Limit.Get("no_exist", "no_exist", 20))
		assert.True(t, Limit.Get("no_exist", "no_exist", 20))
		assert.True(t, Limit.Get("no_exist", "no_exist", 20))
		assert.True(t, Limit.Get("no_exist", "no_exist", 20))
		assert.False(t, Limit.Get("no_exist", "no_exist", 20))

		Limit.Put("no_exist", "no_exist")
		assert.True(t, Limit.Get("no_exist", "no_exist", 20))
	*/
}

func get(user, alias string) error {
	ch, err := Limit.Get(user, alias)
	if err != nil {
		return err
	}

	select {
	case <-ch:
		return nil
	case <-time.After(time.Millisecond * 100):
		return fmt.Errorf("获取超时,%s, %s", user, alias)
	}
}

func put(user, alias string) {
	Limit.Put(user, alias)
}
