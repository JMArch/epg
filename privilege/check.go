package privilege

import "github.com/pingcap/tidb/mysql"

//Checker check the user privilege
type Checker interface {
	Auth(user string, auth []byte, salt []byte) (bool, error)
	HasPrivilege(user, db, table string, p mysql.PrivilegeType) (bool, error)
}

//Notifier notify someone NewUser
type Notifier interface {
	NotifyNew(user string, backlog int64)
}
