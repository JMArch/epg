package privilege

import (
	"fmt"
	"strings"
)

func parse(user string) (string, string, error) {
	arr := strings.Split(user, "@")
	if len(arr) != 2 {
		return "", "", fmt.Errorf("user format error: name@host , %v", user)
	}
	name := arr[0]
	host := arr[1]
	return name, host, nil
}

func match(s string, p string) bool {
	if s == "" || p == "" {
		return false
	}
	var lens, lenp = len(s), len(p)
	var p1, p2, m = 0, 0, 0
	for p1 < lens && p2 < lenp {
		if p[p2] == '\\' {
			p2++
			continue
		}
		if p[p2] == '_' {
			p1++
			p2++
			continue
		}
		if p[p2] == '%' {
			p2++
			m = p2
			continue
		}
		if s[p1] != p[p2] {
			if p1 == 0 && p2 == 0 {
				return false
			}
			p1 -= p2 - m - 1
			p2 = m
			continue
		}
		p1++
		p2++
	}
	if p2 == lenp {
		if p1 == lens || p[p2-1] == '%' {
			return true //两个字符串都结束了，说明模式匹配成功 ||str1还没有结束，但pattern的最后一个字符是_，所以匹配成功
		}
	}
	for p2 < lenp {
		if p[p2] != '%' { //pattern多出的字符只要有一个不是_,匹配失败
			return false
		}
		p2++
	}
	return true
}
