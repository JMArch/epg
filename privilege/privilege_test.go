package privilege

import "testing"

var user = "root"
var pass = "root"
var addr = "127.0.0.1:3306"

// func TestAuth(t *testing.T) {

// 	salt, _ := hex.DecodeString("44114F067C6C236F5F0440522F2F74203241254E")
// 	//stored, _ := hex.DecodeString("81F5E21E35407D884A6CD4A731AEBFB6AF209E1B")
// 	token, _ := hex.DecodeString("170904541A8897755AE104684B7F4ADDBDB23DA0")

// 	auth := NewUserChecker(addr, user, pass)
// 	if auth == nil {
// 		t.Error("NewUserChecker error")
// 		return
// 	}
// 	ok := auth.Auth("root@localhost", salt, token)
// 	if !ok {
// 		t.Error("error")
// 		return
// 	}
// 	t.Log("OK")
// }

// func TestGetPriv(t *testing.T) {
// 	checker := NewUserChecker(addr, user, pass)
// 	if checker == nil {
// 		t.Error("NewUserChecker error")
// 		return
// 	}
// 	_, err := checker.getPriv("newuser", "localhost")
// 	if err != nil {
// 		t.Error(err)
// 		return
// 	}
// 	t.Log("OK")
// }

// func TestGetAuthPriv(t *testing.T) {
// 	checker := NewUserChecker(addr, user, pass)
// 	if checker == nil {
// 		t.Error("NewUserChecker error")
// 		return
// 	}
// 	_, err := checker.getAuthPriv("newuser", "localhost")
// 	if err != nil {
// 		t.Error(err)
// 		return
// 	}
// 	t.Log("OK")
// }

type cases struct {
	in   []string
	want bool
}

var input = []cases{
	cases{in: []string{"abc", "abc"}, want: true},
	cases{in: []string{"abc_", "abcd"}, want: true},
	cases{in: []string{"abc%", "abcd"}, want: true},
	cases{in: []string{"ab_%", "abcd"}, want: true},
	cases{in: []string{"%ab%", "aabb"}, want: true},
	cases{in: []string{"%ab", "bbab"}, want: true},
	cases{in: []string{"127.0.%.%", "127.0.0.1"}, want: true},
	cases{in: []string{"127.%.%.%", "127.0.0.1"}, want: true},
	cases{in: []string{`order\_%`, "order_0"}, want: true},
	cases{in: []string{`order\%`, "order%"}, want: true},
}

func TestMatch(t *testing.T) {
	for _, v := range input {
		r := match(v.in[1], v.in[0])
		if r != v.want {
			t.Fatalf("Fatal: %v", v)
		}
	}
	t.Log("OK")
}

func BenchmarkMatch(b *testing.B) {
	for i := 0; i < b.N; i++ {
		v := input[i%10]
		r := match(v.in[1], v.in[0])
		if r != v.want {
			b.Fatalf("Fatal: %v", v)
		}
	}
}
