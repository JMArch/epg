package api

//Status
const (
	StatusOK        uint8 = 0
	StatusErr       uint8 = 1
	StatusClientErr uint8 = 2
)

// health
const (
	NodeServicing uint8 = 1
	NodeStop      uint8 = 2
	NodePause     uint8 = 3
)

// syncPriv
const(	
	SyncNone uint8 = 1
	SyncFin  uint8 = 2
	SyncIng  uint8 = 3
	SyncErr  uint8 = 4
)

//Reply api reply struct
type Reply struct {
	Host   string
	Status uint8
	Msg    string
	Time   int64
}
