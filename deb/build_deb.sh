#!/bin/bash -e
GO=go
AppName="epg"
Version=0.5.6
BuildTime=`date -u '+%Y-%m-%d_%I:%M:%S%p'`
GitHash=`git rev-parse --short HEAD`
LDFlag="-w -s -X main.GitHash=${GitHash} -X main.BUILD_DATE=${BuildTime} -X main.VERSION=${Version}"

echo "version: ${Version}"
echo "GitHash: ${GitHash}"
echo "BuildTime: ${BuildTime}"
echo "`${GO} version`"

#enter dir
cd ..

#pack a release version.
echo "build bin/${AppName}, epgowl"
env GOOS=linux GOARCH=amd64 CGO_ENABLED=0 ${GO} build -ldflags="${LDFlag}" -o bin/${AppName} epg
env GOOS=linux GOARCH=amd64 CGO_ENABLED=0 ${GO} build -ldflags="${LDFlag}" -o bin/epgowl ./tools/epgowl/
cp -f bin/${AppName} deb/epg/usr/bin/
cp -f bin/epgowl deb/epg/usr/bin/
cp -f conf/epg.toml deb/epg/etc/epg/


echo "build deb..."
cd ./deb/
#sudo dpkg -b ${AppName} ${AppName}_${Version}.deb
rsync -rv  --exclude=.DS_Store epg root@192.168.17.16:/home/zhuliang
ssh -l root 192.168.17.16 "cd /home/zhuliang && dpkg -b epg epg_"${Version}".deb"
rsync root@192.168.17.16:/home/zhuliang/epg_${Version}.deb .
echo "finish"
