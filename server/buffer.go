package server

import (
	"bytes"
	"sync"
)

var pool = &sync.Pool{
	New: func() interface{} {
		buf := make([]byte, defaultBufSize)
		return bytes.NewBuffer(buf)
	},
}

type bufPool struct {
	buf *bytes.Buffer
}

func newBufPool() *bufPool {
	p := &bufPool{
		buf: pool.Get().(*bytes.Buffer),
	}
	return p
}

//GetSize reset the buffer and return it. so it will clear all the before.
func (p *bufPool) GetSize(size int) (buf []byte) {
	p.buf.Reset()
	if p.buf.Cap() < size {
		p.buf.Grow(size)
	}
	return (p.buf.Bytes())[:size]
}

func (p *bufPool) Get() (buf *bytes.Buffer) {
	p.buf.Reset()
	return p.buf
}

func (p *bufPool) Put() {
	pool.Put(p.buf)
	p.buf = nil
}
