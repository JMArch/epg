package server

import "time"

//Client Connection sys setting
const (
	rcvBuffer          = 32767
	sndBuffer          = 65535
	clientWriteTimeout = 60 * time.Second  //second
	clientReadDeadline = 120 * time.Second //second
)

//buffer io
const (
	defaultBufSize    = 8 * 1024 //gourutinue alloc []byte buffer pool
	defaultReaderSize = 8 * 1024 //connection bufio reader buffer szie
)

var (
	clientBaseID = uint32(1000) //atomic client connection id
)

//MysqlDB
var (
	dbMaxConn      = 10 * 1024              //最大连接数chan 大小
	//dbRetryTimeout = 500 * time.Millisecond //获取连接最大等待时间
	dbReleaseTick  = 5 * time.Second        //连接释放时间间隔
)

var (
	sqlSetStmt = map[string]bool{
		// "autocommit":            true,
		"character_set_results": true,
		"collation_connection":  true,
		"sql_mode":              true,
		"SetNAMES":              true,
	}
)
