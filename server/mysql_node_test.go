package server

import (
	"fmt"
	"testing"
)

func TestGCD(t *testing.T) {
	cases := []int{100, 20, 30, 45}

	t.Log(gcd(cases))
}

func TestBlance(t *testing.T) {
	db1 := &MysqlDB{alias: "db1"}
	db2 := &MysqlDB{alias: "db2"}

	p := newDBPool(2)
	p.put(db1, 100)
	p.put(db2, 100)

	for i := 0; i < 100; i++ {
		db := p.Balance()
		fmt.Println(db.alias)
	}
}
