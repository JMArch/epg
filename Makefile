.PHONY: all bin default newver parser

BuildTime=`date -u '+%Y-%m-%d_%I:%M:%S%p'`
GitHash=`git rev-parse --short HEAD`

ARCH      := "`uname -s`"
LINUX     := "Linux"
MAC       := "Darwin"

default : build

#enable profile:
#Memory profile (http://host:8080/debug/pprof/heap)
#CPU profile (http://host:6060/debug/pprof/profile)
#Goroutine blocking profile (http://host:6060/debug/pprof/block)

build : #only for dev.
	#sudo cp -f ./etc/epg.toml /etc/epg/epg.toml
	go build -v -race -ldflags="-X main.GitHash=${GitHash} -X main.BUILD_DATE=${BuildTime} -X main.VERSION=Dev" \
		-o bin/epg epg

parser:
	cd parser && go run goyacc/main.go ./parser.y && rm -f y.output
