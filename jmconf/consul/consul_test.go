package consul

import (
	. "epg/jmconf"
	"fmt"
	"github.com/hashicorp/consul/api"
	"log"
	"testing"
)

var (
	config = &DisCfg{}

	keyConf  = "/jumeiconf/Res/EPG/Inst2/Conf"
	keyNodes = "/jumeiconf/Res/EPG/Inst2/Nodes"
)

func TestGetAndWatch(t *testing.T) {
	p, err := NewCSParser(config)
	ch := make(chan Opt)
	err = p.GetAndWatch(keyConf, ch)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(<-ch)
}

func TestListAndWatch(t *testing.T) {
	p, err := NewCSParser(config)
	ch := make(chan []Opt)
	err = p.ListAndWatch(keyNodes, ch)
	if err != nil {
		log.Fatal(err)
	}
	for e := range ch {
		for _, v := range e {
			fmt.Println("===========")
			fmt.Println(v)
		}
		return
	}
}

var (
	s1 = api.KVPairs{
		&api.KVPair{Key: "/nodes/001", ModifyIndex: 1, Value: nil},
		&api.KVPair{Key: "/nodes/002", ModifyIndex: 2, Value: nil},
		&api.KVPair{Key: "/nodes/003", ModifyIndex: 3, Value: nil},
		&api.KVPair{Key: "/nodes/004", ModifyIndex: 4, Value: nil},
	}
	s2 = api.KVPairs{
		&api.KVPair{Key: "/nodes/001", ModifyIndex: 10, Value: nil},
		&api.KVPair{Key: "/nodes/002", ModifyIndex: 2, Value: nil},
		&api.KVPair{Key: "/nodes/003", ModifyIndex: 3, Value: nil},
		&api.KVPair{Key: "/nodes/005", ModifyIndex: 11, Value: nil},
	}
)

func TestDiff(t *testing.T) {
	df := diff(s1, s2)
	fmt.Println(df)
}

func BenchmarkDiff(b *testing.B) {
	for i := 0; i < b.N; i++ {
		diff(s1, s2)
	}
}
