package etcd

// import (
// 	"time"

// 	"github.com/BurntSushi/toml"
// 	"github.com/coreos/etcd/client"
// 	"golang.org/x/net/context"
// )

// import (
// 	"epg/log"
// )

// //EtcdParser parse etcd config
// type EtcdParser struct {
// 	LocalCfg
// }

// var _ Parser = &EtcdParser{}

// //Parse parse config from etcd.
// func (ep *EtcdParser) Parse(config interface{}) (chan struct{}, error) {
// 	cfg := client.Config{
// 		Endpoints:               ep.Addrs,
// 		Transport:               client.DefaultTransport,
// 		HeaderTimeoutPerRequest: time.Second * time.Duration(ep.Timeout),
// 	}
// 	c, err := client.New(cfg)
// 	if err != nil {
// 		log.Error(err)
// 		return nil, err
// 	}
// 	kapi := client.NewKeysAPI(c)

// 	resp, err := kapi.Get(context.Background(), ep.Path, nil)
// 	if err != nil {
// 		log.Error(err)
// 		return nil, err
// 	}
// 	log.Debugf("Get is done. Metadata is %q\n", resp)
// 	log.Debugf("%q key has %q value\n", resp.Node.Key, resp.Node.Value)

// 	if err := toml.Unmarshal([]byte(resp.Node.Value), &config); err != nil {
// 		log.Error(err)
// 		return nil, err
// 	}
// 	return nil, nil
// }
