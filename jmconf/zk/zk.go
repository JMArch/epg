package zk

// import (
// 	"epg/log"
// 	"fmt"
// 	"reflect"
// 	"time"

// 	"encoding/json"

// 	"github.com/BurntSushi/toml"
// 	"github.com/samuel/go-zookeeper/zk"
// )

// const (
// 	doveStatsOK = 1
// 	doveDeleted = 1
// )

// //ZKParser parse zk config
// type ZKParser struct {
// 	LocalCfg
// 	Data struct {
// 		Content  string `json:"content"`
// 		Metadata struct {
// 			ContentType string `json:"content_type"`
// 			Deleted     int    `json:"deleted"`
// 			Mtime       int    `json:"mtime"`
// 			Ptime       int    `json:"ptime"`
// 			State       int    `json:"state"`
// 		} `json:"metadata"`
// 	}
// }

// var _ Parser = &ZKParser{}

// //Parse parse config from zk.
// func (z *ZKParser) Parse(v interface{}) (chan struct{}, error) {
// 	rv := reflect.ValueOf(v)
// 	if rv.Kind() != reflect.Ptr || rv.IsNil() {
// 		return nil, fmt.Errorf("%v", rv)
// 	}

// 	cli, _, err := zk.Connect(z.Addrs, time.Duration(z.Timeout)*time.Second)
// 	if err != nil {
// 		log.Error(err)
// 		return nil, err
// 	}
// 	data, _, evt, err := cli.GetW(z.Path)
// 	if err != nil {
// 		log.Error(err)
// 		return nil, err
// 	}
// 	log.Infof("Load ZK Config: %+v", z.Path)
// 	if err := json.Unmarshal(data, &z.Data); err != nil {
// 		log.Error(err)
// 		return nil, err
// 	}

// 	//check the dove state and deleted
// 	if z.Data.Metadata.State != doveStatsOK || z.Data.Metadata.Deleted == doveDeleted {
// 		return nil, fmt.Errorf("dove Metadata Stats error: %+v", z.Data.Metadata)
// 	}

// 	if err = toml.Unmarshal([]byte(z.Data.Content), v); err != nil {
// 		return nil, err
// 	}

// 	evtCh := make(chan struct{}, 1)
// 	go func() {
// 		for {
// 			select {
// 			case e := <-evt:
// 				if e.State == zk.StateDisconnected {
// 					cli.Close()
// 					log.Error(e)
// 					return
// 				}
// 				switch e.Type {
// 				case zk.EventNodeDataChanged:
// 					log.Debugf("EventNodeDataChanged: %+v", e)
// 					//notify the data changed.
// 					evtCh <- struct{}{}
// 					return
// 				}
// 			}
// 		}
// 	}()
// 	return evtCh, nil
// }
