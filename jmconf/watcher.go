package jmconf

import "fmt"

//OpType 操作类型
type OpType byte

//操作类型
const (
	OPAdd OpType = 1 + iota
	OPModify
	OPDelete
)

//Opt Changes
type Opt struct {
	Op OpType
	KV
}

func (o Opt) String() string {
	return fmt.Sprintf("OP:%v, KV:%v", o.Op, o.KV.String())
}

//KV the path and value pairs
type KV struct {
	Key     string
	Val     []byte
	LastIdx uint64
}

func (k KV) String() string {
	return fmt.Sprintf("KV{Key:%v,Val:%v,LastIdx:%v}", k.Key, string(k.Val), k.LastIdx)
}

//Watcher the remoute config interface
type Watcher interface {
	Close() error
	WatchKey(key string, data chan Opt) (err error)
	WatchPrefix(key string, datas chan []Opt) (err error)
}
